<?php

/**
 * @file
 * Uppercase initial letter, word in plural form.
 */

// --------------------------------- A, a --------------------------------------

t('Accents');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=602
t('Accesses');																																	//
t('Accessibilities');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=2807517
t('Actions');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=786
//'Activateds'
t('Activities');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=126684
t('Ad-blockers');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2755028
t('Adds');																																			//
//'Additionals' https://english.stackexchange.com/questions/430894/is-additionals-a-word
t('Addresses');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=85618
t('Admins');																																		//
t('Administers');																																//
//'Administrations'
t('Administrators');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=65460
t('Ages');																																			//
t('Aggregates');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2818961
//'Aggregations'
t('Aggregators');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2762458
t('Agreements');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2669632
t('Albums');																																		//
t('Alerts');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=193134
//'Alls'
t('Alphabets');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2797020
t('Anchors');																																		//
//'Ands'
//'Anies'
t('Anonymizers');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2807518
//'Anonymizings'
//'Anonymouses'
t('Approvals');																																	//
t('Approves');																																	//
//'Aquamarines'
t('Archives');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4514
t('Arguments');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=22124
t('Articles');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1300
t('Artists');																																		//
t('Ashes');																																			//
//'Assigneds'
t('Attachments');																																//
t('Attributes');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=19220
t('Audiences');																																	//
//'Audios
t('Authentications');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=2757330
t('Authorings');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2755444
t('Authorizations');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=270846
//'Authorizeds'
t('Authors');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=66870
//'Autocompleteds'
t('Autocompletes');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2757331
//'Availables'
t('Avatars');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=16006


// --------------------------------- B, b --------------------------------------

//'Backs'																																				//
t('Backgrounds');																																//
t('Bins');
t('Binaries');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2757332
t('Bitrates');																																	//
t('Blanks');																																		//
t('Blocks');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=69086
t('Blogs');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=44524
//'Bluemarines'
t('Boards');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2755512
t('Bodies');																																		//
t('Books');																																			//
t('Booleans');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762459
t('Brands');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2614684
t('Breadcrumbs');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=285668
t('Breaks');																																		//
t('Breakpoints');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1369243
//'Browsable'
t('Builds');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=118422
t('Bundles');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=172992
//'Bies'


// --------------------------------- C, c --------------------------------------

t('Caches');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1534573
t('Cacheables');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2757333
t('Calculations');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2797021
t('Callbacks');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1143428
t('Calms');																																			//
t('Cancels');																																		//
//'Canceleds'
t('Captchas');																																	//
t('Cardinalities');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2807519
t('Carts');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2464318
t('Catalogs');																																	//
t('Categories');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2064
t('Centers');																																		//
t('Changes');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=3682
t('Changelogs');																																//
t('Checklists');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=886189
t('Checkouts');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2757334
t('Chessboards');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2757335
t('Classes');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=77390
t('Clicks');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=21196
//'Closeds'
t('Clouds');																																		//
t('Codes');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=111124
//'Coders'
//'Collapseds'
t('Comments');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1322
t('Compacts');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2757336
//'Completeds'
t('Complexes');																																	//
t('Components');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=6906
t('Configurations');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=39056
t('Configures');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2757337
t('Confirms');																																	//
t('Confirmations');																															//
//'Confirmeds'
t('Connectors');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=459723
t('Consents');																																	//
t('Contains');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=10912
t('Containers');																																//
t('Contents');																																	//
t('Contexts');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=84740
//'Contribs'
t('Contributors');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=178592
t('Controls');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=38296
t('Cookies');																																		//
t('Cores');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1404688
t('Costs');																																			//
t('Countries');																																	//
//'Createds'
t('Crons');																																			//
t('Currencies');																																//
t('Customs');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2757338


// --------------------------------- D, d --------------------------------------

t('Databases');																																	//
t('Dates');																																			//
t('Days');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=39568
//'Dailies'
t('Declines');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2757339
t('Defaults');																																	//
//'Deletes'
t('Denials');																																		//
t('Denies');																																		//
t('Dependencies');																															//
//'Deploys'
t('Depths');																																		//
//'Deprecateds'
t('Descriptions');																															//
t('Developers');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2012
t('Developments');																															//
t('Dimensions');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=5230
t('Directories');																																//
t('Disables');																																	//
//'Disableds'
t('Displays');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=235160
t('Distributions');																															//
//'Documentations'
t('Domains');																																		//
t('Downloads');																																	//
//'Downloadings'
t('Drafts');																																		//
t('Dropbuttons');																																//
t('Dropzones');																																	//
//'Drupals'
t('Duplicates');																																//


// --------------------------------- E, e --------------------------------------

t('E-mails');																																		//
//'Easts'
//'Edits'
t('Educations');																																//
//'Eights'
t('Elements');																																	//
t('Emails');																																		//
t('Empties');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2757340
t('Enables');																																		//
//'Enableds'
t('Encodings');																																	//
t('Ends');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4796
t('Endpoints');																																	//
t('Energies');																																	//
//'Enforceds'
t('Enters');																																		//
t('Entities');																																	//
t('Environments');																															//
t('Errors');																																		//
t('Events');																																		//
t('Exits');																																			//
t('Expands');																																		//
//'Expandeds'
t('Expires');																																		//
//'Expireds'
//'Explicits'
t('Exports');																																		//
//'Exported'
//'Exporting'
//'Extends'
t('Extensions');																																//


// --------------------------------- F, f --------------------------------------

t('Faces');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=762
t('Facets');																																		//
t('Facilities');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1048614
//'Falses'
t('Features');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=35168
//'Featureds'
t('Feeds');																																			//
t('Fields');																																		//
t('Fieldsets');																																	//
t('Files');																																			//
t('Filenames');																																	//
t('Filters');																																		//
//'Finalizations'
//'Fives'
t('Flags');																																			//
t('Focuses');																																		//
t('Folders');																																		//
t('Fonts');																																			//
t('Footers');																																		//
//'Fors'
t('Forms');																																			//
t('Formats');																																		//
t('Formatters');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=501368
t('Formulas');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762460
t('Forums');																																		//
//'Fours'
//'Fridays'
t('Fulfillments');																															//
t('Fullscreens');																																//
t('Functions');																																	//
//'Fuzzies'


// --------------------------------- G, g --------------------------------------

t('Galleries');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4874
t('Gateways');																																	//
t('Genres');																																		//
t('Gets');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=137538
t('Goes');																																			//
//'Grays'
//'Greens'
//'Greenbeam'
//'Greys'
//'Grosses
t('Groups'); t('Group(s)');																											// https://localize.drupal.org/translate/languages/hu/translate?sid=3484


// --------------------------------- H, h --------------------------------------

t('Hashes');																																		//
//'Hasheds'
t('Hashids');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2305208
t('Hashtags');																																	//
t('Headers');																																		//
t('Headlines');																																	//
t('Heights');																																		//
t('Helps');																																			//
t('Heros');																																			//
//'Hiddens'
t('Hierarchies');																																//
//'Highs'
t('Hints');																																			//
//'Histories'
t('Hits');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=59866
//'Homes'
t('Homepages');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762462
t('Hooks');																																			//
t('Hosts');																																			//
t('Hostnames');																																	//
t('Hours');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=41472
t('Hourlies');																																	//
t('HTMLs');																																			//


// --------------------------------- I, i --------------------------------------

t('Icons');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6356
t('Images');																																		//
//'Imperials'
t('Imports');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2209633
//'Importeds'
//'Importings'
t('Impressums');																																//
t('Industries');																																//
//'Informations'
//'Inits'
//'Initializings'
t('Inputs');																														  			//
t('Inserts');																																		//
t('Installs');																																	//
t('Installations');																															//
//'Installeds'
t('Instances');																																	//
t('Integrations');																															//
t('Interests');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=88474
t('Interfaces');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=502053
t('Intersections');																															//
//'Ises'
t('Issues');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6878
t('Items');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=11774


// --------------------------------- J, j --------------------------------------

t('Joins');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=284142


// --------------------------------- K, k --------------------------------------

t('Keys');																																			//
t('Keyboards');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762463
t('Keywords');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=6768
//'Khakis'


// --------------------------------- L, l --------------------------------------

t('Labels');																																		//
t('Languages');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=35128
//'Larges'
t('Layout');																																		//
t('Leaderboards');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2762464
//'Legals'
t('Lengths');																																		//
t('Libraries');																																	//
t('Licenses');																																	//
t('Limits');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=62214
t('Links');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2348
t('Liquids');																																		//
t('Lists');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=36286
//'Listens'
t('Locales');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=251092
t('Logs');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2588
//'Loggings'
t('Logins');																																		//
t('Logos');																																			//
//'Lows'
//'Lowercases'


// --------------------------------- M, m --------------------------------------

t('Mails');																																			//
//'Mains'
//'Majors'
//'Manages'
t('Managers');																																	//
t('Manufacturers');																															//
t('Mappings');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=204898
//'Marines'
t('Markups');																																		//
t('Masks');
t('Materials');																																	//
t('Maximums');																																	//
//'Mediterranos'
t('Members');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=15790
t('Memberships');																																//
t('Menus');																																			//
//'Mercuries'
t('Messages');																																	//
t('Messagings');																																//
t('Metrics');																																		//
//'Midnights'
t('Minimums');																																	//
//'Minors'
t('Minutes');																																		//
t('Missions');																																	//
t('Modules');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5378
//'Mondays'
t('Months');																																		//
//'Monthlies'
//'Mores'
t('Moves');																																			//
//'Multiples'


// --------------------------------- N, n --------------------------------------

t('Names');																																			//
//'Nevers'
t('News');																																			//
//'Nexts'
//'Nines'
//'Nos'
//'Nocturnals'
t('Nodes');																																			//
//'Nones'
//'Noons'
//'Norths'
//'Northeast'
//'Northwests'
t('Notes');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6192
t('Numbers');																																		//


// --------------------------------- O, o --------------------------------------

//'Obsoletes'
//'Olivias'
//'Ons
//'Ones'
//'Opens'
t('Operations');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=398
t('Operators');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1051904
t('Options');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=7786
//'Optionals'
//'Oranges'
t('Orphans');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=3522
t('Outlines');																																	//
//'Overalls'
//'Overcasts'
t('Overviews');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762466


// --------------------------------- P, p --------------------------------------

t('Pages');																																			//
t('Pagers');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2586004
t('Panes');																																			//
t('Parents');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=3012
//'Partials'																																		//
t('Participants');																															//
t('Passwords');																																	//
t('Patches');																																		//
//'Patch-levels'
t('Paths');																																			//
t('Payers');																																		//
t('Permissions');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=46834
t('Photos');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=11086
//'Pinks'
t('Places');																																		//
t('Placeholders');																															//
t('Platforms');																																	//
t('Players');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5034
t('Playlists');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4632
t('Plugins');																																		//
t('Podcasts');																																	//
t('Popups');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=170460
t('Posts');																																			//
//'Pre-authorizeds'
t('Prefixes');																																	//
t('Previews');																																	//
//'Previouses
t('Priorities');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=6908
t('Privates');																																	//
t('Processes');																																	//
t('Profiles');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=77388
t('Programs');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4630
//'Programmings'
//'Progress'
t('Projects');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2042
t('Providers');																																	//
//'Publics'
//'Publishs'
//'Publisheds'
//'Purchaseds'
t('Purchasers');																																//
//'Purples'
//'Pxs'


// --------------------------------- Q, q --------------------------------------

t('Quantities');																																//
t('Queries');																																		//
t('Quotas');																																		//
t('Quotes');																																		//


// --------------------------------- R, r --------------------------------------

//'Radioactivities'
t('Rains');																																			//
//'Randoms'
t('Ranges');																																		//
//'Readies'
//'Reduces'
//'Refreshes'
t('Regions');																																		//
t('Registers');																																	//
t('Releases');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2182
//'Relevancies'
t('Removes');																																		//
//'Renames'
t('Replies');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=690
//'Requesteds'
//'Requireds'
//'Resets'
t('Resources');																																	//
t('Responses');																																	//
t('Results');																																		//
t('Reviews');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5374
t('Roles');																																			//
t('Roots');																																			//


// --------------------------------- S, s --------------------------------------

t('Salts');																																			//
//'Sanitizes'
//'Saturdays'
//'Saves'
//'Says'
t('Schedules');																																	//
t('Schemes');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2468
t('Screens');																																		//
t('Screenshots');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1944
t('Searches');																																	//
t('Seconds');																																		//
t('Sections');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=75708
//'Sees'
t('Selects');																																		//
t('Selectors');																																	//
//'Selves'
//'Serializations'
//'Serializeds'
t('Serializers');																																//
t('Servers');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1898
t('Services');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=81170
t('Sessions');																																	//
t('Sets');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=62868
t('Settings');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=3054
t('Setups');																																		//
//'Sevens'
//'Shippeds'
t('Shops');																																			//
t('Singles');																																		//
t('Sites');																																			//
t('Sizes');																																			//
//'Sixes');
t('Slideshows');																																//
//'Smalls'
t('Snippets');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=5524
t('Sources');																																		//
//'Souths'
//'Southeasts'
//'Southwests'
//'Stagings'
t('Starts');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4794
//'Statics'
t('Stations');																																	//
t('Statistics');																																//
t('Statuses');																																	//
t('Stickies');																																	//
t('Stories');																																		//
t('Streams');																																		//
t('Strings');																																		//
t('Styles');																																		//
t('Subjects');																																	//
t('Submits');																																		//
t('Subscribes');																																//
t('Subscribers');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=758
t('Subtotals'); t('Subtotals:');																								// https://localize.drupal.org/translate/languages/hu/translate?sid=270676
t('Subversions');																																//
t('Suffixes');																																	//
t('Suggestions');																																//
t('Summaries');																																	//
//'Sundays'
t('Supports');																																	//
//'Syncs'
t('Synchronizations');																													//
//'Synchronizes'
t('Synonyms');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2986


// --------------------------------- T, t --------------------------------------

t('Tabs');																																			//
t('Tags');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1604
t('Tasks');																																			//
t('Taxonomies');																																//
t('Teasers');																																		//
t('Templates');																																	//
//'Tens'
t('Tests');																																			//
//'Testings'
t('Texts');																																			//
t('Themes');																																		//
//'Threes');
//'Thursdays'
t('Times');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4780
t('Timestamps');																																//
t('Titles');																																		//
t('Tokens');																																		//
t('Tools');																																			//
t('Toolbars');																																	//
t('Toolkits');																																	//
//'Tops'
t('Totals');																																		//
t('Tracks');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4618
t('Transactions');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=568
t('Translations');																															//
t('Transliterates');																														//
//'Transliterateds'
//'Trues'
//'Tuesdays'
//'Twos'
t('Types');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2970


// --------------------------------- U, u --------------------------------------

//'Unauthorizeds'
//'Unassigns'
//'Unassigneds'
//'Unavailables'
//'Unconfirmeds'
//'Undos'
//'Uninstalleds'
//'Uniques'
//'Unknowns'
t('Unmasquerades');																															//
//'Unmasqueradeds'
//'Unpublisheds'
//'Unscheduleds'																																//
t('Unsubscribes');																															//
//'Untranslateds'
//'Ups'
t('Updates');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6788
//'Updateds'
//'Uppercases'
t('Uptimes');																																		//
t('URLs');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1450
t('Users');																																			//
t('Usernames');																																	//


// --------------------------------- V, v --------------------------------------

t('Validators');																																//
t('Values');																																		//
t('Variables');																																	//
//'VATs'
//'Verboses'
t('Verifies');																																	//
t('Versions');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=6904
t('Views');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4864
t('Viewports');																																	//
//'Visibles'
t('Vocabularies');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=5344
t('Volunteers');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=6228


// --------------------------------- W, w --------------------------------------

t('Weathers');																																	//
t('Websites');																																	//
//'Wednesdays'
t('Weeks');																																			//
//'Weeklies'
t('Weights');																																		//
//'Wests'
t('Widgets');																																		//
//'Widths'
//'Wikis'
t('Wishlists');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1388
t('Workspaces');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2438384


// --------------------------------- X, x --------------------------------------

//…


// --------------------------------- Y, y --------------------------------------

t('Years');																																			//
//'Yeses'



// --------------------------------- Z, z --------------------------------------

t('Zooms');																																			//
