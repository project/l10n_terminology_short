<?php

/**
 * @file
 * Uppercase initial letter, word in singular form.
 */

// --------------------------------- A, a --------------------------------------

t('Accent');																																		//
t('Access');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4866
t('Accessibility');																															//
t('Action');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1444
t('Activated');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=8390
t('Activity');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=35746
t('Ad-blocker');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2755023
t('Add');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=4876
t('Additional');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=26000
t('Address');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=14350
t('Admin');																																			//
t('Administer');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=338
t('Administration');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=1314
t('Administrator');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=83448
t('Age');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=892
t('Aggregate');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=84770
t('Aggregation');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=5864
t('Aggregator');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=89090
t('Agreement');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=266320
t('Album');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4558
t('Alert'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=270326 */ t('ALERT'); // https://localize.drupal.org/translate/languages/hu/translate?sid=1474558
t('All');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=8872
t('Alphabet');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=102958
t('Anchor');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5740
t('And');																																				//
t('Any');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=23954
t('Anonymizer');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2670705
t('Anonymizing');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2807514
t('Anonymous');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=21534
t('Approval');																																	//
t('Approve');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=388
t('Aquamarine');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1708
t('Archive');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4462
t('Argument');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=21490
t('Article');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1264
t('Artist');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4562
t('Ash');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=1706
t('Assigned'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=6832 */ t('Assigned:'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=6874 */
t('Attachment'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=6990 */ t('Attachment:');																							// https://localize.drupal.org/translate/languages/hu/translate?sid=6790
t('Attribute');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=524
t('Audience');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=968
t('Audio');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=5030
t('Authentication');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=53716
t('Authoring');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=239714
t('Authorization');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=101730
t('Authorized');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=270850
t('Author');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=688
t('Autocompleted');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=74694
t('Autocomplete');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=171020
t('Available');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=84704
t('Avatar');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=28594


// --------------------------------- B, b --------------------------------------

t('Back');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3464
t('Background');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=3950
t('Bin');																																				//
t('Binary');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=44254
t('Bitrate');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5080
t('Blank');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4212
t('Block');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=9306
t('Blog');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=19772
t('Bluemarine');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1712
t('Board');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=674379
t('Body');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=114
t('Book');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3298
t('Boolean');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=171642
t('Brand');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=83822
t('Breadcrumb');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=9448
t('Break');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2938
t('Breakpoint');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1368083
t('Browsable');																																	//
t('Build');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=90758
t('Bundle');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=173058
t('By'); t('By ');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=580


// --------------------------------- C, c --------------------------------------

t('Cache');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=21424
t('Cacheable');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=10604
t('Calculation');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=49142
t('Callback');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=231400
t('Calm');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2860
t('Cancel');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=812
t('Canceled');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=71374
t('Captcha');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2714
t('Cardinality');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=106140
t('Cart');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=48848
t('Catalog');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4556
t('Category'); t('Category:'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=6870 */
t('Center');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2940
t('Change');																																		//
t('Changelog');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1948
t('Checklist');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=573679
t('Checkout');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=21580
t('Chessboard');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=40646
t('Class');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=20146
t('Click');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=175242
t('Closed');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=704
t('Cloud');																																			//
t('Code');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=14760
t('Coder');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=5402
t('Collapsed');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=26084
t('Comment'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=10572 */ t('Comment', [], ['context' => 'Comment Button']); // https://localize.drupal.org/translate/languages/hu/translate?sid=2646679
t('Compact');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=239018
t('Completed');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=50040
t('Complex');																																		//
t('Component'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=6800 */ t('Component:'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=6868 */
t('Configuration');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=18544
t('Configure');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=7658
t('Confirm');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=810
t('Confirmation');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=27526
t('Confirmed');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1956353
t('Connector');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=459443
t('Consent');																																		//
t('Contain');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1355763
t('Container');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2958
t('Content');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=400
t('Context');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=93262
t('Contrib');																																		//
t('Contributor');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=552309
t('Control');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=169966
t('Cookie');																																		//
t('Core');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4370
t('Cost');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1446
t('Country');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2792
t('Created');																																		//
t('Cron');																																			//
t('Currency');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1352
t('Custom');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=10560


// --------------------------------- D, d --------------------------------------

t('Database');																																	//
t('Date');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2344
t('Day');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=17676
t('Daily');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2442
t('Decline');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=118126
t('Default'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=3920 */ t('Default:');																										// https://localize.drupal.org/translate/languages/hu/translate?sid=3888
t('Delete');																																		//
t('Denial');																																		//
t('Deny');																																			//
t('Dependency');																																//
t('Deploy');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=81124
t('Depth');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3018
t('Deprecated');																																//
t('Description');																																//
t('Developer');																																	//
t('Development');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=630
t('Dimension');																																	//
t('Directory');																																	//
t('Disable');																																		//
t('Disabled');																																	//
t('Display'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=5760 */ t('Display:');																																			//
t('Distribution');																															//
t('Documentation');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=1936
t('Domain');																																		//
t('Download');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2070
t('Downloading');																																//
t('Draft'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=2731401 */ t('Draft', [], ['context' => 'Workflow state label']); // https://localize.drupal.org/translate/languages/hu/translate?sid=2731401
t('Dropbutton');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1736228
t('Dropzone');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2610489
t('Drupal');																																		//
t('Duplicate');																																	//


// --------------------------------- E, e --------------------------------------

t('E-mail');																																		//
t('East');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2866
t('Edit');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2342
t('Education');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=232648
t('Eight');																																			//
t('Element');																																		//
t('Email');																																			//
t('Empty');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=39048
t('Enable');																																		//
t('Enabled');																																		//
t('Encoding');																																	//
t('End');																																				//
t('Endpoint');																																	//
t('Energy');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=369268
t('Enforced');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2660645
t('Enter');																																			//
t('Entity');																																		//
t('Environment');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=56970
t('Error'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=7778 */ t('ERROR'); // https://localize.drupal.org/translate/languages/hu/translate?sid=1760
t('Event');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6260
t('Exit');																																			//
t('Expand');																																		//
t('Expanded');																																	//
t('Expire');																																		//
t('Expired');																																		//
t('Explicit');																																	//
t('Export');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=3300
t('Exported');																																	//
t('Exporting');																																	//
t('Extend'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=40480 */ t('Extend', [], ['context' => 'With components']); // https://localize.drupal.org/translate/languages/hu/translate?sid=2067338
t('Extension');																																	//


// --------------------------------- F, f --------------------------------------

t('Face');																																			//
t('Facet');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=57004
t('Facility');																																	//
t('False');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3872
t('Feature');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=35204
t('Featured');																																	//
t('Feed');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3064
t('Field');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3490
t('Fieldset');																																	//
t('File');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2252
t('Filename');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=5260
t('Filter');																																		//
t('Finalization');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=960469
t('Five');																																			//
t('Flag');																																			//
t('Focus');																																			//
t('Folder');																																		//
t('Font');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3904
t('Footer');																																		//
t('For');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=1462
t('Form');																																			//
t('Format');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4966
t('Formatter');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=115460
t('Formula');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2067878
t('Forum');																																			//
t('Four');																																			//
t('Friday');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4360
t('Fulfillment');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=291966
t('Fullscreen');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=233548
t('Function');																																	//
t('Fuzzy');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2179628


// --------------------------------- G, g --------------------------------------

t('Gallery');																																		//
t('Gateway');																																		//
t('Genre');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4670
t('Get'); t('GET');																															//
t('Go');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=2080
t('Gray'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=54642 */ t('GRAY'); // https://localize.drupal.org/translate/languages/hu/translate?sid=308382
t('Green'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=75466 */ t('Green'); // https://localize.drupal.org/translate/languages/hu/translate?sid=308384
t('Greenbeam');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1718
t('Grey');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=79836
t('Gross');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=544
t('Group');																																			//


// --------------------------------- H, h --------------------------------------

t('Hash');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=656684
t('Hashed');																																		//
t('Hashid');																																		//
t('Hashtag');																																		//
t('Header');																																		//
t('Headline');																																	//
t('Height');																																		//
t('Help');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3720
t('Hero');																																			//
t('Hidden');																																		//
t('Hierarchy');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2974
t('High');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4426
t('Hint');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=278818
t('History');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5096
t('Hit');																																				//
t('Home');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=66
t('Homepage');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1932
t('Hook');																																			//
t('Host');																																			//
t('Hostname');																																	//
t('Hour');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=37604
t('Hourly');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2440
t('HTML');																																			//


// --------------------------------- I, i --------------------------------------

t('Icon');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4692
t('Image');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2936
t('Imperial');																																	//
t('Import');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=3296
t('Imported');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=7744
t('Importing');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=132526
t('Impressum');																																	//
t('Industry');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=783644
t('Information');																																//
t('Init');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1709508
t('Initializing'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=367413 */ t('Initializing.'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=224696 */ t('Initializing...'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=449978 */
t('Input');																																			//
t('Insert');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4088
t('Install');																																		//
t('Installation');																															//
t('Installed');																																	//
t('Instance');																																	//
t('Integration');																																//
t('Interest');																																	//
t('Interface');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=259334
t('Intersection');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=59178
t('Is');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=668
t('Issue');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=7002
t('Item');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=558


// --------------------------------- J, j --------------------------------------

t('Join');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=58422


// --------------------------------- K, k --------------------------------------

t('Key');																																				//
t('Keyboard');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=952389
t('Keyword');																																		//
t('Khaki');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2678177


// --------------------------------- L, l --------------------------------------

t('Label');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3676
t('Language');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=932
t('Large');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4280
t('Layout');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6390
t('Leaderboard');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=21074
t('Legal');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3648
t('Length');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4964
t('Library');																																		//
t('License');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1940
t('Limit');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=36330
t('Link');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2934
t('Liquid');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4052
t('List');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=760
t('Listen');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4756
t('Locale');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=10618
t('Log');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=976
t('Logging');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1684
t('Login');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2294
t('Logo');																																			//
t('Low');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=4428
t('Lowercase');																																	//


// --------------------------------- M, m --------------------------------------

t('Mail');																																			//
t('Main');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4926
t('Major');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2240
t('Manage');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5624
t('Manager'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=886 */ t('Manager: '); // https://localize.drupal.org/translate/languages/hu/translate?sid=1058
t('Manufacturer');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=15102
t('Mapping');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=79500
t('Marine');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=359330
t('Markup');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=354
t('Mask');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=244650
t('Material');																																	//
t('Maximum');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=13050
t('Mediterrano');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1720
t('Member');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=110798
t('Membership');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=47968
t('Menu');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=5788
t('Mercury');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1722
t('Message');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2602
t('Messaging');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=19416
t('Metric');																																		//
t('Midnight');																																	//
t('Minimum');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=70016
t('Minute');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=37606
t('Minor');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2242
t('Mission');																																		//
t('Module');																																		//
t('Monday');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4352
t('Month');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=13718
t('Monthly');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2446
t('More');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1390
t('Move');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4056
t('Multiple');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2978


// --------------------------------- N, n --------------------------------------

t('Name');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3114
t('Never');																																			//
t('New');																																				//
t('Next');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=130
t('Nine');																																			//
t('No');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=1690
t('Nocturnal');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1724
t('Node');																																			//
t('None'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=2476 */ t('- None -');	// https://localize.drupal.org/translate/languages/hu/translate?sid=2760
t('Noon');																																			//
t('North');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2862
t('Northeast');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2864
t('Northwest');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2876
t('Note'); t('Note:');																													// https://localize.drupal.org/translate/languages/hu/translate?sid=3100
t('Number');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2600


// --------------------------------- O, o --------------------------------------

t('Obsolete');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2349138
t('Olivia');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1726
t('On'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=1460 */ t('On '); // https://localize.drupal.org/translate/languages/hu/translate?sid=3628
t('One');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=3070
t('Open');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4200
t('Operation');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=24742
t('Operator');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=22126
t('Option');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=22110
t('Optional');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=22132
t('Orange'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=79838 */ t('ORANGE'); // https://localize.drupal.org/translate/languages/hu/translate?sid=308386
t('Orphan');																																		//
t('Outline');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=3520
t('Overall');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6804
t('Overcast');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2834
t('Overview');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2184


// --------------------------------- P, p --------------------------------------

t('Page');																																			//
t('Pager');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=93324
t('Pane');																																			//
t('Parent');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=3008
t('Partial');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4202
t('Participant');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=6912
t('Password');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2646
t('Patch');																																			//
t('Patch-level');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2244
t('Path');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=5172
t('Payer');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=564
t('Permission');																																//
t('Photo');																																			//
t('Pink'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=69646 */ t('PINK'); // https://localize.drupal.org/translate/languages/hu/translate?sid=308388
t('Place');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2796
t('Placeholder');																																//
t('Platform');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=234636
t('Player');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5132
t('Playlist');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4596
t('Plugin');																																		//
t('Podcast');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4762
t('Popup');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=108210
t('Post');																																			//
t('Pre-authorized');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=1925738
t('Prefix');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=356
t('Preview');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=3684
t('Previous');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=126
t('Priority'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=1368 */ t('Priority:'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=6872 */
t('Private');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=586
t('Process');																																		//
t('Profile');																																		//
t('Program');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4610
t('Programming');																																//
t('Progress');																																	//
t('Project'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=2052 */ t('Project:'); // https://localize.drupal.org/translate/languages/hu/translate?sid=192876
t('Provider');																																	//
t('Public');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=962
t('Publish');																																		//
t('Published');																																	//
t('Purchased');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1498
t('Purchaser');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1458
t('Purple'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=54646 */ t('PURPLE'); // https://localize.drupal.org/translate/languages/hu/translate?sid=308390
//'Px'


// --------------------------------- Q, q --------------------------------------

t('Quantity');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1448
t('Query');																																			//
t('Quota');																																			//
t('Quote');																																			//


// --------------------------------- R, r --------------------------------------

t('Radioactivity');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=210946
t('Rain');																																			//
t('Random');																																		//
t('Range', [], ['context' => 'Validation']);																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1864988
t('Ready');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=272658
t('Reduce');																																		//
t('Refresh');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5642
t('Region');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5688
t('Register');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=344
t('Release');																																		//
t('Relevancy');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4290
t('Remove');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=820
t('Rename');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6426
t('Reply');																																			//
t('Requested');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1494
t('Required');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2998
t('Reset');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2410
t('Resource');																																	//
t('Response');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2604
t('Result');																																		//
t('Review');																																		//
t('Role');																																			//
t('Root');																																			//


// --------------------------------- S, s --------------------------------------

t('Salt');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=61182
t('Sanitize');																																	//
t('Saturday');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4362
t('Save');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3686
//'Say'
t('Schedule');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4730
t('Scheme');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2484
t('Screen');																																		//
t('Screenshot');																																//
t('Search');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2360
t('Second');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=37608
t('Section');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5498
t('See');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=1570
t('Select');																																		//
t('Selector');																																	//
t('Self');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4214
t('Serialization');																															//
t('Serialized');																																//
t('Serializer');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1756053
t('Server');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=81098
t('Service');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=81422
t('Session');																																		//
t('Set');																																				//
t('Setting');																																		//
t('Setup');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4872
t('Seven');																																			//
t('Shipped');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=408633
t('Shop');																																			//
t('Single');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2976
t('Site');																																			//
t('Size');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2346
t('Six');																																				//
t('Slideshow');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=106
t('Small');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4278
t('Snippet');																																		//
t('Source');																																		//
t('South');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2870
t('Southeast');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2868
t('Southwest');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2872
t('Staging');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=41728
t('Start'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=17784 */ t('Start:'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=17884 */
t('Static');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=23878
t('Station');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4366
//'Statistic'
t('Status'); t('Status:'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=6876 */
t('Sticky');																																		//
t('Story');																																			//
t('Stream');																																		//
t('String');																																		//
t('Style');																																			//
t('Subject');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=770
t('Submit');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=396
t('Subscribe');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=848
t('Subscriber');																																//
t('Subtotal'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=49744 */ t('Subtotal:'); // https://localize.drupal.org/translate/languages/hu/translate?sid=140350
t('Subversion');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=105618
t('Suffix');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=358
t('Suggestion');																																//
t('Summary');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=3972
t('Sunday');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4350
t('Support');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2000
t('Sync');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=5340
t('Synchronization');																														//
t('Synchronize');																																//
t('Synonym');																																		//


// --------------------------------- T, t --------------------------------------

t('Tab');																																				//
t('Tag');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=2258
t('Task');																																			//
t('Taxonomy');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1648
t('Teaser');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5970
t('Template');																																	//
t('Ten');																																				//
t('Test');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2586
t('Testing');																																		//
t('Text');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6354
t('Theme');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6386
t('Three');																																			//
t('Thursday');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4358
t('Time');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4818
t('Timestamp');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=6764
t('Title');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=112
t('Token');																																			//
t('Tool');																																			//
t('Toolbar');																																		//
t('Toolkit');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1766578
t('Top');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=4216
t('Total');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6812
t('Track');																																			//
t('Transaction');																																//
t('Translation');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=68818
t('Transliterate');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=252450
t('Transliterated');																														//
t('True');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3870
t('Tuesday');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4354
t('Two');																																				//
t('Type');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=686


// --------------------------------- U, u --------------------------------------

t('Unauthorized');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=50712
t('Unassign');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=6826
t('Unassigned');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=6828
t('Unavailable');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=81426
t('Unconfirmed');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=244532
t('Undo');																																			//
t('Uninstalled');																																//
t('Unique');																																		//
t('Unknown');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6980
t('Unmasquerade');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2430458
t('Unmasqueraded');																															//
t('Unpublished');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=58510
t('Unscheduled');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=4792
t('Unsubscribe');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=860
t('Untranslated');																															//
t('Up');																																				//
t('Update');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4094
t('Updated');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6304
t('Uppercase'); t('UPPERCASE');																									// https://localize.drupal.org/translate/languages/hu/translate?sid=3922
t('Uptime');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=200676
t('URL');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=5130
t('User');																																			//
t('Username');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=538


// --------------------------------- V, v --------------------------------------

t('Validator');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=136622
t('Value');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=526
t('Variable');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2880
t('VAT');																																				//
t('Verbose');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=18246
t('Verify');																																		//
t('Version');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2072
t('View');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4954
t('Viewport');																																	//
t('Visible');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5694
t('Vocabulary');																																//
t('Volunteer');																																	//


// --------------------------------- W, w --------------------------------------

t('Weather');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2762
t('Website');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=46498
t('Wednesday');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4356
t('Week');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=17672
t('Weekly');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2444
t('Weight');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2812
t('West');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2874
t('Widget');																																		//
t('Width');																																			//
t('Wiki');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4110
t('Wishlist');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1324
t('Workspace');


// --------------------------------- X, x --------------------------------------

//…


// --------------------------------- Y, y --------------------------------------

t('Year');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=9188
t('Yes');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=1688



// --------------------------------- Z, z --------------------------------------

t('Zoom');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=115388
