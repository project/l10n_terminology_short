<?php

$variable = 2;

formatPlural($variable, '1 alert', '@count alerts', ['@count' => 2], []);				// https://localize.drupal.org/translate/languages/hu/translate?sid=2468208
formatPlural($variable, '1 item', '@count items', ['@count' => 2], []);					// https://localize.drupal.org/translate/languages/hu/translate?sid=32178
formatPlural($variable, 'item', 'items', ['@count' => 2], []);									// https://localize.drupal.org/translate/languages/hu/translate?sid=31282
