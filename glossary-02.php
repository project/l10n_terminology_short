<?php

/**
 * @file
 * Lowercase initial letter, word in plural form.
 */

// --------------------------------- A, a --------------------------------------

t('accents');																																		//
t('accesses');																																	//
t('accessibilities');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=2807490
t('actions');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6314
//'activateds'
t('activities');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1633038
t('ad-blockers');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2755003
t('adds');																																			//
//'additionals'
t('addresses');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=141258
t('admins');																																		//
t('administers');																																//
//'administrations'
t('administrators');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=224100
t('ages');																																			//
t('aggregates');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2818944
//'aggregations'
t('aggregators');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2762414
t('agreements');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2762415
t('albums');																																		//
t('alerts');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2755511
//'alls'
t('alphabets');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2796999
t('anchors');																																		//
//'ands'
//'anies'
t('anonymizers');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2807491
//'anonymizings'
//'anonymouses'
t('approvals');																																	//
t('approves');																																	//
//'aquamarines'
t('archives');																																	//
t('arguments');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=75190
t('articles');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=6662
t('artists');																																		//
t('ashes');																																			//
//'assigneds'
t('attachments');																																//
t('attributes');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=673784
t('audiences');																																	//
//'audios
t('authentications');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=2757315
t('authorings');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2755441
t('authorizations');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=2755442
//'authorizeds'
t('authors');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2755443
//'autocompleteds'
t('autocompletes');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2757316
//'availables'
t('avatars');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2818945


// --------------------------------- B, b --------------------------------------

//'backs'																																				//
t('backgrounds');																																//
t('bins');
t('binaries');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2757317
t('bitrates');																																	//
t('blanks');																																		//
t('blocks');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1284088
t('blogs');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6536
//'bluemarines'
t('boards');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2755514
t('bodies');																																		//
t('books');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6556
t('booleans');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762418
t('brands');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2762416
t('breadcrumbs');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=948839
t('breaks');																																		//
t('breakpoints');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1369243
//'browsable'
t('builds');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2762417
t('bundles');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1930293
//'bies'


// --------------------------------- C, c --------------------------------------

t('caches');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2755515
t('cacheables');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2757318
t('calculations');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2797000
t('callbacks');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2757319
t('calms');																																			//
t('cancels');																																		//
//'canceleds'
t('captchas');																																	//
t('cardinalities');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2807492
t('carts');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2762419
t('catalogs');																																	//
t('categories');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=3476
t('centers');																																		//
t('changes');																																		//
t('changelogs');																																//
t('checklists');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2762420
t('checkouts');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2757320
t('chessboards');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2757321
t('classes');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1938993
t('clicks');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=16444
//'closeds'
t('clouds');																																		//
t('codes');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=132322
//'coders'
//'collapseds'
t('comments');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=26750
t('compacts');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2757322
//'completeds'
t('complexes');																																	//
t('components');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2159528
t('configurations');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=1380888
t('configures');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2757323
t('confirms');																																	//
t('confirmations');																															//
//'confirmeds'
t('connectors');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2757324
t('consents');																																	//
t('contains');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=95246
t('containers');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=3478
t('contents');																																	//
t('contexts');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=535329
//'contribs'
t('contributors');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2757325
t('controls');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1078493
t('cookies');																																		//
t('cores');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2757326
t('costs');																																			//
t('countries');																																	//
//'createds'
t('crons');																																			//
t('currencies');																																//
t('customs');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2757327


// --------------------------------- D, d --------------------------------------

t('databases');																																	//
t('dates');																																			//
t('days');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=8894
//'dailies'
t('declines');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2757328
t('defaults');																																	//
//'deletes'
t('denials');																																		//
t('denies');																																		//
t('dependencies');																															//
//'deploys'
t('depths');																																		//
//'deprecateds'
t('descriptions');																															//
t('developers');																																//
t('developments');																															//
t('dimensions');																																//
t('directories');																																//
t('disables');																																	//
//'disableds'
t('displays');																																	//
t('distributions');																															//
//'documentations'
t('domains');																																		//
t('downloads');																																	//
//'downloadings'
t('drafts');																																		//
t('dropbuttons');																																//
t('dropzones');																																	//
//'drupals'
t('duplicates');																																//


// --------------------------------- E, e --------------------------------------

t('e-mails');																																		//
//'easts'
//'edits'
t('educations');																																//
//'eights'
t('elements');																																	//
t('emails');																																		//
t('empties');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2757329
t('enables');																																		//
//'enableds'
t('encodings');																																	//
t('ends');																																			//
t('endpoints');																																	//
t('energies');																																	//
//'enforceds')
t('enters');																																		//
t('entities');																																	//
t('environments');																															//
t('errors');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=7756
t('events');																																		//
t('exits');																																			//
t('expands');																																		//
//'expandeds'
t('expires');																																		//
//'expireds'
//'explicits'
t('exports');																																		//
//'exporteds'
//'exportings'
//'extends'
t('extensions');																																//


// --------------------------------- F, f --------------------------------------

t('faces');																																			//
t('facets');																																		//
t('facilities');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2762422
//'falses'
t('features');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=302808
//'featureds'
t('feeds');																																			//
t('fields');																																		//
t('fieldsets');																																	//
t('files');																																			//
t('filenames');																																	//
t('filters');																																		//
//'finalizations'
//'fives'
t('flags');																																			//
t('focuses');																																		//
t('folders');																																		//
t('fonts');																																			//
t('footers');																																		//
//'fors'
t('forms');																																			//
t('formats');																																		//
t('formatters');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=866464
t('formulas');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762424
t('forums');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5626
//'fours'
//'fridays'
t('fulfillments');																															//
t('fullscreens');																																//
t('functions');																																	//
//'fuzzies'


// --------------------------------- G, g --------------------------------------

t('galleries');																																	//
t('gateways');																																	//
t('genres');																																		//
t('gets');
t('goes');																																			//
//'grays'
//'greens'
//'greenbeam'
//'greys'
//'grosses
t('groups');																																		//


// --------------------------------- H, h --------------------------------------

t('hashes');																																		//
//'hasheds'
t('hashids');																																		//
t('hashtags');																																	//
t('headers');																																		//
t('headlines');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=6498
t('heights');																																		//
t('helps');																																			//
t('heros');																																			//
//'hiddens'
t('hierarchies');																																//
//'highs'
t('hints');																																			//
//'histories'
t('hits');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=59856
//'homes'
t('homepages');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762427
t('hooks');																																			//
t('hosts');																																			//
t('hostnames');																																	//
t('hours');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=8888
t('hourlies');																																	//
//'htmls'


// --------------------------------- I, i --------------------------------------

t('icons');																																			//
t('images');																																		//
//'imperials'
t('imports');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2762428
//'importeds'
//'importings'
t('impressums');																																//
t('industries');																																//
//'informations'
//'inits'
//'initializings'
t('inputs');																																		//
t('inserts');																																		//
t('installs');																																	//
t('installations');																															//
//'installeds'
t('instances');																																	//
t('integrations');																															//
t('interests');																																	//
t('interfaces');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2762429
t('intersections');																															//
t('items');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=11342
t('issues');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6972
//'ises'


// --------------------------------- J, j --------------------------------------

t('joins');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=283836


// --------------------------------- K, k --------------------------------------

t('keys');																																			//
t('keyboards');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762430
t('keywords');																																	//
//'khakis'


// --------------------------------- L, l --------------------------------------

t('labels');																																		//
t('languages');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=333698
//'larges'
t('layouts');																																		//
t('leaderboards');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2762431
//'legals'
t('lengths');																																		//
t('libraries');																																	//
t('licenses');																																	//
t('limits');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2762432
t('links');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=20018
t('liquids');																																		//
t('lists');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=579524
//'listens'
t('locales');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2762433
t('logs');																																			//
//'loggings'
t('logins');																																		//
t('logos');																																			//
//'lows'
//'lowercases'


// --------------------------------- M, m --------------------------------------

t('mails');																																			//
//'mains'
//'majors'
//'manages'
t('managers');																																	//
t('manufacturers');																															//
t('mappings');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=937844
//'marines'
t('markups');																																		//
t('masks');
t('materials');																																	//
t('maximums');																																	//
//'mediterranos'
t('members');																																		//
t('memberships');																																//
t('menus');																																			//
//'mercuries'
t('messages');																																	//
t('messagings');																																//
t('metrics');																																		//
//'midnight'
t('minimums');																																	//
//'minors'
t('minutes');																																		//
t('missions');																																	//
t('modules');																																		//
//'monday'
t('months');																																		//
//'monthlies'
//'mores'
t('moves');																																			//
//'multiples'


// --------------------------------- N, n --------------------------------------

t('names');																																			//
//'nevers'
t('news');																																			//
//'nexts'
//'nines'
//'nos'
//'nocturnal'
t('nodes');																																			//
//'nones'
//'noon'
//'norths'
//'northeast'
//'northwests'
t('notes');
t('numbers');																																		//


// --------------------------------- O, o --------------------------------------

//'obsoletes'
//'olivias'
//'ons
//'ones'
//'opens'
t('operations');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=14820
t('operators');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762435
t('options');																																		//
//'optionals'
//'oranges'
t('orphans');																																		//
t('outlines');																																	//
//'overalls'
//'overcasts'
t('overviews');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762436


// --------------------------------- P, p --------------------------------------

t('pages');																																			//
t('pagers');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2665660
t('panes');																																			//
t('parents');																																		//
//'partials'																																		//
t('participants');																															//
t('passwords');																																	//
t('patches');																																		//
//'patch-levels'
t('paths');																																			//
t('payers');																																		//
t('permissions');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=168852
t('photos');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4
//'pinks'
t('places');																																		//
t('placeholders');																															//
t('platforms');																																	//
t('players');																																		//
t('playlists');																																	//
t('plugins');																																		//
t('podcasts');																																	//
t('popups');																																		//
t('posts');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=45282
//'pre-authorizeds'
t('prefixes');																																	//
t('previews');																																	//
//'previouses
t('priorities');																																//
t('privates');																																	//
t('processes');																																	//
t('profiles');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=334874
t('programs');																																	//
//'programmings'
//'progresses'
t('projects');																																	//
t('providers');																																	//
//'publics'
//'publishs'
//'publisheds'
//'purchaseds'
t('purchasers');																																//
//'purples'
//'pxs'


// --------------------------------- Q, q --------------------------------------

t('quantities');																																//
t('queries');																																		//
t('quotas');																																		//
t('quotes');																																		//


// --------------------------------- R, r --------------------------------------

//'radioactivities'
t('rains');																																			//
//'randoms'
t('ranges');																																		//
//'readies'
//'reduces'
//'refreshes'
t('regions');																																		//
t('registers');																																	//
t('releases');																																	//
//'relevancies'
t('removes');																																		//
//'renames'
t('replies');																																		//
//'requesteds'
//'requireds'
//'resets'
t('resources');																																	//
t('responses');																																	//
t('results');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5822
t('reviews');																																		//
t('roles');																																			//
t('roots');																																			//


// --------------------------------- S, s --------------------------------------

t('salts');																																			//
//'sanitizes'
//'saturdays'
//'saves'
t('says:');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3630
t('schedules');																																	//
t('schemes');																																		//
t('screens');																																		//
t('screenshots');																																//
t('searches');																																	//
t('seconds');																																		//
t('sections');																																	//
//'sees'
t('selects');																																		//
t('selectors');																																	//
//'selves'
//'serializations'
//'serializeds'
t('serializers');																																//
t('servers');																																		//
t('services');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=81154
t('sessions');																																	//
t('sets');																																			//
t('settings');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=3434
t('setups');																																		//
//'sevens'
//'shippeds'
t('shops');																																			//
t('singles');																																		//
t('sites');																																			//
t('sizes');																																			//
//'sixes');
t('slideshows');																																//
//'smalls'
t('snippets');																																	//
t('sources');																																		//
//'souths'
//'southeasts'
//'southwests'
//'stagings'
//'starts'
//'statics'
t('stations');																																	//
t('statistics');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=6940
t('statuses');																																	//
t('stickies');																																	//
t('stories');																																		//
t('streams');																																		//
t('strings');																																		//
t('styles');																																		//
t('subjects');																																	//
t('submits');																																		//
t('subscribes');																																//
t('subscribers');																																//
t('subtotals');																																	//
t('subversions');																																//
t('suffixes');																																	//
t('suggestions');																																//
t('summaries');																																	//
//'sundays'
t('supports');																																	//
//'syncs'
t('synchronizations');																													//
//'synchronizes'
t('synonyms');																																	//


// --------------------------------- T, t --------------------------------------

t('tabs');																																			//
t('tags'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=12 */ t('tags: '); /* https://localize.drupal.org/translate/languages/hu/translate?sid=16 */
t('tasks');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6882
t('taxonomies');																																//
t('teasers');																																		//
t('templates');																																	//
//'tens'
t('tests');																																			//
//'testings'
t('texts');																																			//
t('themes');																																		//
//'threes');
//'thursdays'
t('times');																																			//
t('timestamps');																																//
t('titles');																																		//
t('tokens');																																		//
t('tools');																																			//
t('toolbars');																																	//
t('toolkits');																																	//
//'tops'
t('totals');																																		//
t('tracks');																																		//
t('transactions');																															//
t('translations');																															//
t('transliterates');																														//
//'transliterateds'
//'trues'
//'tuesdays'
//'twos'
t('types');																																			//


// --------------------------------- U, u --------------------------------------

//'unauthorizeds'
//'unassigns'
//'unassigneds'
//'unavailables'
//'unconfirmeds'
//'undos'
//'uninstalleds'
//'uniques'
//'unknowns'
t('unmasquerades');																															//
//'unmasqueradeds'
//'unpublisheds'
//'unscheduleds'																																//
t('unsubscribes');																															//
//'untranslateds'
//'ups'
t('updates');																																		//
//'updateds'
//'uppercases'
t('uptimes');																																		//
t('urls');																																			//
t('users');																																			//
t('usernames');																																	//


// --------------------------------- V, v --------------------------------------

t('validators');																																//
t('values');																																		//
t('variables');																																	//
//'vats'
//'verboses'
t('verifies');																																	//
t('versions');																																	//
t('views');																																			//
t('viewports');																																	//
//'visibles'
t('vocabularies');																															//
t('volunteers');																																//


// --------------------------------- W, w --------------------------------------

t('weathers');																																	//
t('websites');																																	//
//'wednesdays'
t('weeks');																																			//
//'weeklies'
t('weights');																																		//
//'wests'
t('widgets');																																		//
//'widths'
//'wikis'
t('wishlists');																																	//
t('workspaces');


// --------------------------------- X, x --------------------------------------

//…


// --------------------------------- Y, y --------------------------------------

t('years');																																			//
//'yeses'



// --------------------------------- Z, z --------------------------------------

t('zooms');																																			//
