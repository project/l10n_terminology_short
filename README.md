This project is not a regular module or theme for Drupal. Instead, it's special
purpose is to provide further groupings of UI strings helping translators
working on localize.drupal.org. For more details about see the documentation:
https://www.drupal.org/docs/8/modules/using-cat-tools-for-drupal.
