<?php

/**
 * @file
 * Lowercase initial letter, word in singular form.
 */

// --------------------------------- A, a --------------------------------------

t('accent');																																		//
t('access');																																		//
t('accessibility');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=1024474
t('action');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=7644
t('activated');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=53470
t('activity');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=35710
t('ad-blocker');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2754992
t('add');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=6418
t('additional');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2818933
t('address');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=48786
t('admin');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=884
t('administer');																																//
t('administration');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=592709
t('administrator');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=874
t('age');																																				//
t('aggregate');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2818934
t('aggregation');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=5998
t('aggregator');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=6492
t('agreement');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1792033
t('album');																																			//
t('alert');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=229846
t('all'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=2204 */ t('<all>'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=6934 */
t('alphabet');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2796985
t('anchor');																																		//
t('and');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=4430
t('any');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=39286
t('anonymizer');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2807479
t('anonymizing');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2807480
t('anonymous');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=23954
t('approval');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=404
t('approve');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=366
t('aquamarine');																																//
t('archive');																																		//
t('argument');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=93564
t('article');																																		//
t('artist');																																		//
t('ash');																																				//
t('assigned');																																	//
t('attachment');																																//
t('attribute');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=342604
t('audience');																																	//
t('audio');																																			//
t('authentication');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=87580
t('authoring');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2755439
t('authorization');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=468648
t('authorized');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1925913
t('author');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=38830
t('autocompleted');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2757312
t('autocomplete');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=5122
t('available');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=257646
t('avatar');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1112913


// --------------------------------- B, b --------------------------------------

t('back');																																			//
t('background');																																//
t('bin');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=1982613
t('binary');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=730224
t('bitrate');																																		//
t('blank');																																			//
t('block');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=30656
t('blog');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=7342
t('bluemarine');																																//
t('board');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2755513
t('body');																																			//
t('book');																																			//
t('boolean');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=307098
t('brand');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1880108
t('breadcrumb');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2530
t('break');																																			//
t('breakpoint');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=1369238
t('browsable');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=5126
t('build');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2633040
t('bundle');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1462828
t('by');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=7384


// --------------------------------- C, c --------------------------------------

t('cache');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=283084
t('cacheable');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2757313
t('calculation');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2344468
t('callback');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=675254
t('calm');																																			//
t('cancel');																																		//
t('canceled');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=52294
t('captcha');																																		//
t('cardinality');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2689779
t('cart');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=14166
t('catalog');																																		//
t('category');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=3498
t('center');																																		//
t('change');																																		//
t('changelog');																																	//
t('checklist');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=886364
t('checkout');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=35012
t('chessboard');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=40638
t('class');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=22650
t('click');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=214956
t('closed');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=780
t('cloud');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=14
t('code');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=14812
t('coder');																																			//
t('collapsed');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=69954
t('comment');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=58282
t('compact');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=13306
t('completed');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=52296
t('complex');																																		//
t('component');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1074743
t('configuration');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=53712
t('configure');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=11758
t('confirm');																																		//
t('confirmation');																															//
t('confirmed');																																	//
t('connector');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=545749
t('consent');																																		//
t('contain');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=191414
t('container'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=3500 */ t('[container]'); // https://localize.drupal.org/translate/languages/hu/translate?sid=3540
t('content');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=610
t('context');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=312
t('contrib');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2100
t('contributor');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=516659
t('control');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1078488
t('cookie');																																		//
t('core');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2098
t('cost');																																			//
t('country');																																		//
t('created');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2174
t('cron');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1698
t('currency');																																	//
t('custom');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=110606


// --------------------------------- D, d --------------------------------------

t('database');																																	//
t('date'); t('!date');																													// https://localize.drupal.org/translate/languages/hu/translate?sid=2364
t('day');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=37560
t('daily');																																			//
t('decline');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2048753
t('default');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2496
t('delete');																																		//
t('denial');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=406
t('deny');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=882
t('dependency');																																//
t('deploy');																																		//
t('depth');																																			//
t('deprecated');																																//
t('description');																																//
t('developer');																																	//
t('development');																																//
t('dimension');																																	//
t('directory');																																	//
t('disable');																																		//
t('disabled');																																	//
t('display');																																		//
t('distribution');																															//
t('documentation');																															//
t('domain');																																		//
t('download');																																	//
t('downloading');																																//
t('draft');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=374003
t('dropbutton');																																//
t('dropzone');																																	//
t('drupal');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1664
t('duplicate');																																	//


// --------------------------------- E, e --------------------------------------

t('e-mail');																																		//
t('east');																																			//
t('edit');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3124
t('education');																																	//
t('eight');																																			//
t('element');																																		//
t('email');																																			//
t('empty');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=87266
t('enable');																																		//
t('enabled');																																		//
t('encoding');																																	//
t('end');																																				//
t('endpoint');																																	//
t('energy');																																		//
t('enforced');																																	//
t('enter');																																			//
t('entity');																																		//
t('environment');																																//
t('error');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1582
t('event');																																			//
t('exit');																																			//
t('expand');																																		//
t('expanded');																																	//
t('expire');																																		//
t('expired');																																		//
t('explicit');																																	//
t('export');																																		//
t('exported');																																	//
t('exporting');																																	//
t('extend');																																		//
t('extension');																																	//


// --------------------------------- F, f --------------------------------------

t('face');																																			//
t('facet');																																			//
t('facility');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762399
t('false');																																			//
t('feature');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=637944
t('featured');																																	//
t('feed');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1036
t('field');																																			//
t('fieldset');																																	//
t('file');																																			//
t('filename');																																	//
t('filter');																																		//
t('finalization');																															//
t('five');																																			//
t('flag');																																			//
t('focus');																																			//
t('folder');																																		//
t('font');																																			//
t('footer');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1320
t('for');																																				//
t('form');																																			//
t('format');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6402
t('formatter');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=342520
t('formula');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2762400
t('forum');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=5610
t('four');																																			//
t('friday');																																		//
t('fulfillment');																																//
t('fullscreen');																																//
t('function');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=300
t('fuzzy');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2762401


// --------------------------------- G, g --------------------------------------

t('gallery');																																		//
t('gateway');																																		//
t('genre');																																			//
t('get');
t('go');																																				//
t('gray');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=579424
t('green');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=230036
t('greenbeam');																																	//
t('grey');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2607925
t('gross');																																			//
t('group');																																			//


// --------------------------------- H, h --------------------------------------

t('hash');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=303104
t('hashed');																																		//
t('hashid');																																		//
t('hashtag');																																		//
t('header');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=3638
t('headline');																																	//
t('height');																																		//
t('help');																																			//
t('hero');																																			//
t('hidden');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5124
t('hierarchy');																																	//
t('high');																																			//
t('hint');																																			//
t('history');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=180008
t('hit');																																				//
t('home');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=13384
t('homepage');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2755707
t('hook');																																			//
t('host');																																			//
t('hostname');																																	//
t('hour');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=37566
t('hourly');																																		//
t('html');																																			//


// --------------------------------- I, i --------------------------------------

t('icon');																																			//
t('image');																																			//
t('imperial');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2810
t('import');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=96228
t('imported');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=7748
t('importing');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762402
t('impressum');																																	//
t('industry');																																	//
t('information');																																//
t('init');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2752829
t('initializing');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2762403
t('input');																																			//
t('insert');																																		//
t('install');																																		//
t('installation');																															//
t('installed');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=3156
t('instance');																																	//
t('integration');																																//
t('interest');																																	//
t('interface');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=303138
t('intersection');																															//
t('is');																																				//
t('issue');																																			//
t('item');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=11340


// --------------------------------- J, j --------------------------------------

t('join');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=349536


// --------------------------------- K, k --------------------------------------

t('key');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=5716
t('keyboard');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1050139
t('keyword');																																		//
t('khaki');																																			//


// --------------------------------- L, l --------------------------------------

t('label');																																			//
t('language');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=249632
t('large');																																			//
t('layout');																																		//
t('leaderboard');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=48502
t('legal');																																			//
t('length');																																		//
t('library');																																		//
t('license');																																		//
t('limit');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=920699
t('link');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=5718
t('liquid');																																		//
t('list');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=8
t('listen');																																		//
t('locale');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=200234
t('log');																																				//
t('logging');																																		//
t('login');																																			//
t('logo');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2540
t('low');																																				//
t('lowercase');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=3924


// --------------------------------- M, m --------------------------------------

t('mail');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6248
t('main');																																			//
t('major');																																			//
t('manage');																																		//
t('manager');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=870
t('manufacturer');																															//
t('mapping');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=937834
t('marine');																																		//
t('markup');																																		//
t('mask');
t('material');																																	//
t('maximum');																																		//
t('mediterrano');																																//
t('member');																																		//
t('membership');																																//
t('menu');																																			//
t('mercury');																																		//
t('message');																																		//
t('messaging');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=199310
t('metric');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2808
t('midnight');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=4346
t('minimum');																																		//
t('minor');																																			//
t('minute');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=29644
t('mission');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2542
t('module');																																		//
t('monday');																																		//
t('month');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=37562
t('monthly');																																		//
t('more');																																			//
t('move');																																			//
t('multiple');																																	//


// --------------------------------- N, n --------------------------------------

t('name');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6416
t('never');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6152
t('new');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=1528
t('next'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=15318 */ t('next ›'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=32802 */ t('Next '); /* https://localize.drupal.org/translate/languages/hu/translate?sid=13894 */ t(' Next >>'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=42298 */ t('Next ›'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=56828 */ t(' Next > '); /* https://localize.drupal.org/translate/languages/hu/translate?sid=95274 */ t('Next >'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=72444 */
t('nine');																																			//
t('no');																																				//
t('nocturnal');																																	//
t('node');																																			//
t('none'); t('(none)'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=3494 */ t('<none>'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=6798 */
t('noon');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4348
t('north');																																			//
t('northeast');																																	//
t('northwest');
t('note');
t('number');																																		//


// --------------------------------- O, o --------------------------------------

t('obsolete');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2665262
t('olivia');																																		//
t('on');																																				//
t('one');																																				//
t('open');																																			//
t('operation');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=164732
t('operator');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2762404
t('option');																																		//
t('optional');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1156
t('orange');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=579419
t('orphan');																																		//
t('outline');																																		//
t('overall');																																		//
t('overcast');																																	//
t('overview');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=13918


// --------------------------------- P, p --------------------------------------

t('page');																																			//
t('pager');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2127633
t('pane');																																			//
t('parent');																																		//
t('partial');																																		//
t('participant');																																//
t('password');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=3730
t('patch');																																			//
t('patch-level');																																//
t('path');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6656
t('payer');																																			//
t('permission');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=47412
t('photo');																																			//
t('pink');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=1698493
t('place');																																			//
t('placeholder');																																//
t('platform');																																	//
t('player');																																		//
t('playlist');																																	//
t('plugin');																																		//
t('podcast');																																		//
t('popup');																																			//
t('post');																																			//
t('pre-authorized');																														// https://localize.drupal.org/translate/languages/hu/translate?sid=2757314
t('prefix');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=123968
t('preview');																																		//
t('previous');																																	//
t('priority');																																	//
t('private');																																		//
t('process');																																		//
t('profile');																																		//
t('program');																																		//
t('programming');																																//
t('progress');																																	//
t('project');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2024
t('provider');																																	//
t('public');																																		//
t('publish');																																		//
t('published');																																	//
t('purchased');																																	//
t('purchaser');																																	//
t('purple');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=580179
t('px');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=3974


// --------------------------------- Q, q --------------------------------------

t('quantity');																																	//
t('query');																																			//
t('quota');																																			//
t('quote');																																			//


// --------------------------------- R, r --------------------------------------

t('radioactivity');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=210944
t('rain');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2854
t('random');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1614
t('range');																																			//
t('ready');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=201588
t('reduce');																																		//
t('refresh');																																		//
t('region');																																		//
t('register');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=2292
t('release');																																		//
t('relevancy');																																	//
t('remove');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4898
t('rename');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=6316
t('reply');																																			//
t('requested');																																	//
t('required');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1158
t('reset');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=19968
t('resource');																																	//
t('response');																																	//
t('result');																																		//
t('review');																																		//
t('role');																																			//
t('root');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=3006


// --------------------------------- S, s --------------------------------------

t('salt');																																			//
t('sanitize');																																	//
t('saturday');																																	//
t('save');																																			//
//'say'
t('schedule');																																	//
t('scheme');																																		//
t('screen');																																		//
t('screenshot');																																//
t('search');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=5826
t('second');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=29640
t('section');																																		//
t('see');																																				//
t('select');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=236
t('selector');																																	//
t('self');																																			//
t('serialization');																															//
t('serialized');																																//
t('serializer');																																//
t('server');																																		//
t('service');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=14774
t('session');																																		//
t('set');	/* https://localize.drupal.org/translate/languages/hu/translate?sid=301373 */ t('set: '); /* https://localize.drupal.org/translate/languages/hu/translate?sid=10 */
t('setting');
t('setup');																																			//
t('seven');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=52288
t('shipped');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=52288
t('shop');																																			//
t('single');																																		//
t('site'); t('!site');																													// https://localize.drupal.org/translate/languages/hu/translate?sid=6274
t('size');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=324
t('six');																																				//
t('slideshow');																																	//
t('small');																																			//
t('snippet');																																		//
t('source');																																		//
t('south');																																			//
t('southeast');																																	//
t('southwest');																																	//
t('staging');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=89708
t('start');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=132946
t('static');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=361544
t('station');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=4432
//'statistic'
t('status');																																		//
t('sticky');																																		//
t('story');																																			//
t('stream');																																		//
t('string');																																		//
t('style');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6398
t('subject');																																		//
t('submit');																																		//
t('subscribe');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=708
t('subscriber');																																//
t('subtotal');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=42430
t('subversion');																																//
t('suffix');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=342718
t('suggestion');																																//
t('summary');																																		//
t('sunday');																																		//
t('support');																																		//
t('sync');																																			//
t('synchronization');																														//
t('synchronize');																																//
t('synonym');																																		//


// --------------------------------- T, t --------------------------------------

t('tab');																																				//
t('tag'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=5120 */ t('@tag'); // https://localize.drupal.org/translate/languages/hu/translate?sid=5140
t('task');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6890
t('taxonomy');																																	//
t('teaser');																																		//
t('template');																																	//
t('ten');																																				//
t('test');																																			//
t('testing');																																		//
t('text');																																			//
t('theme');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=6680
t('three');																																			//
t('thursday');																																	//
t('time');																																			//
t('timestamp');																																	//
t('title');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=316
t('token');																																			//
t('tool');																																			//
t('toolbar');																																		//
t('toolkit');																																		//
t('top');																																				//
t('total');																																			//
t('track');																																			//
t('transaction');																																//
t('translation');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=68748
t('transliterate');																															//
t('transliterated');																														//
t('true');																																			//
t('tuesday');																																		//
t('two');
t('type'); t('@type');																													// https://localize.drupal.org/translate/languages/hu/translate?sid=6292


// --------------------------------- U, u --------------------------------------

t('unauthorized');																															// https://localize.drupal.org/translate/languages/hu/translate?sid=2755440
t('unassign');																																	//
t('unassigned');																																//
t('unavailable');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=42398
t('unconfirmed');																																//
t('undo');																																			//
t('uninstalled');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=3158
t('unique');																																		//
t('unknown');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1464
t('unmasquerade');																															//
t('unmasqueraded');																															//
t('unpublished');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=2170
t('unscheduled');																																//
t('unsubscribe');																																// https://localize.drupal.org/translate/languages/hu/translate?sid=876
t('untranslated');																															//
t('up');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=3238
t('update');																																		//
t('updated');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2172
t('uppercase');																																	//
t('uptime');																																		//
t('url');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=3738
t('user');																																			//
t('username'); /* https://localize.drupal.org/translate/languages/hu/translate?sid=3726 */ t('!username'); // https://localize.drupal.org/translate/languages/hu/translate?sid=1316


// --------------------------------- V, v --------------------------------------

t('validator');																																	//
t('value');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=326
t('variable');																																	//
//'vat'
t('verbose');																																		//
t('verify');																																		//
t('version');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=2300
t('view');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=2102
t('viewport');																																	//
t('visible');																																		//
t('vocabulary');																																//
t('volunteer');																																	//


// --------------------------------- W, w --------------------------------------

t('weather');																																		//
t('website');																																		// https://localize.drupal.org/translate/languages/hu/translate?sid=1062
t('wednesday');																																	//
t('week');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=4856
t('weekly');																																		//
t('weight');																																		//
t('west');																																			//
t('widget');																																		//
t('width');																																			//
t('wiki');																																			//
t('wishlist');																																	// https://localize.drupal.org/translate/languages/hu/translate?sid=1580
t('workspace');																																	//


// --------------------------------- X, x --------------------------------------

//…


// --------------------------------- Y, y --------------------------------------

t('year');																																			// https://localize.drupal.org/translate/languages/hu/translate?sid=37564
t('yes');																																				// https://localize.drupal.org/translate/languages/hu/translate?sid=712



// --------------------------------- Z, z --------------------------------------

t('zoom');																																			//
